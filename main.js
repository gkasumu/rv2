
// GLOBAL VARIABLES //
const parent = this;
console.log(JSON.stringify(document));
console.log(navigator.userAgent);
var webSocket;
var affiliatelist;
var affiliateoption = "";
var affiliateoptionlang = "";
var currentuser;
var bkup_msg = "";
var bkup_footer = "";
var isFaqDisplayed = false;
var isConfirmPageDisplayed = false;
var custLang = "";
parent.fetchAffiliate();
parent.fetchAffiliateLang();
var payload = {
    type: 'SEND_MESSAGE',
    data: {
        user_name: "",
        text: "",
        status: "",
        custLang : parent.custLang
    }
    
}
var selectData = "";
var inputData = "";
var msg_card_body = document.getElementById('msg_card_body');
var clientform = initClientForm;
var prehandform = preHandOverForm;
var mobilemoneyform = mobileMoneyForm;
var submitotpform = submitOtpForm;
// GLOBAL VARIABLES END //


// URLS //

const getaffiliate = "https://rafikiv5.eu-gb.mybluemix.net/proxy/getawaaffiliates";


// URLS END //

// ELEMENTS //
var component = {
    rafikichatbubble: "<div class='d-flex justify-content-start mb-4'><div class='img_cont_msg'><img src='rafiki.png' class='rounded-circle user_img_msg'></div><div class='msg_cotainer'>{message}<span class='msg_time'>{timestamp}</span></div></div>",
    userchatbubble: "<div class='d-flex justify-content-end mb-4'><div class='msg_cotainer_send'> {message} <span class='msg_time_send'>{timestamp}</span></div></div>",
    selectlist: "<div class='form-div' id='countrySelect'><form name='country' onsubmit='handleCountryListSubmit(event)' id='countryForm'><div>Country</div><div><select class='form-control' name='country' required id='affcuntlist'><option value=''>Select country where you are domicilled</option>{lists}</select></div><div><input type='submit' class='btn btn-primary btn-sm' value='Submit'/></div></form></div>",
    inputField: "<input class='form-control' value='' placeholder='' onchange='handlechange(event)'/>",
    buttoncontainer: "<div class='form-div' id='buttoncont'><div class='text-right'>{buttons}</div></div>",
    button: "<div class='text-right'><button class='btn btn-outline-primary btn-sm' onclick='handleButtonOptionClick(event)'>{text}</button></div>",
    optionlist: "<div class='form-div form-list' id='optlist'><ul>{list}</ul></div>",
    istyping: "<div class='d-flex justify-content-start mb-4' id='istyping'><div class='img_cont_msg'><img src='rafiki.png' class='rounded-circle user_img_msg'></div><div><svg class='lds-message' width='100' height='40' viewBox='0 0 100 100' preserveAspectRatio='xMidYMid'><g transform='translate(20 50)'><circle cx='0' cy='0' r='6' fill='#1d3f72' transform='scale(0.128994 0.128994)'>" +
        "<animateTransform attributeName='transform' type='scale' begin='-0.375s' calcMode='spline' keySplines='0.3 0 0.7 1;0.3 0 0.7 1' values='0;1;0' keyTimes='0;0.5;1' dur='1s' repeatCount='indefinite'></animateTransform></circle></g><g transform='translate(40 50)'><circle cx='0' cy='0' r='6' fill='#5699d2' transform='scale(0.00423741 0.00423741)'>" +
        "<animateTransform attributeName='transform' type='scale' begin='-0.25s' calcMode='spline' keySplines='0.3 0 0.7 1;0.3 0 0.7 1' values='0;1;0' keyTimes='0;0.5;1' dur='1s' repeatCount='indefinite'></animateTransform></circle></g><g transform='translate(60 50)'><circle cx='0' cy='0' r='6' fill='#d8ebf9' transform='scale(0.207557 0.207557)'>" +
        "<animateTransform attributeName='transform' type='scale' begin='-0.125s' calcMode='spline' keySplines='0.3 0 0.7 1;0.3 0 0.7 1' values='0;1;0' keyTimes='0;0.5;1' dur='1s' repeatCount='indefinite'></animateTransform></circle></g><g transform='translate(80 50)'><circle cx='0' cy='0' r='6' fill='#71c2cc' transform='scale(0.54949 0.54949)'>" +
        +"<animateTransform attributeName='transform' type='scale' begin='0s' calcMode='spline' keySplines='0.3 0 0.7 1;0.3 0 0.7 1' values='0;1;0' keyTimes='0;0.5;1' dur='1s' repeatCount='indefinite'></animateTransform></circle></g></svg></div></div>"
}
// ELEMENTS END //

function custlangChange(){
    var lang = document.getElementById("languagedropdown");
    console.log(lang.value);
    parent.custLang = lang.value;
    console.log(parent.custLang);
}
function showfaq() {
    var msg_body = document.getElementById("msg_card_body");
    var header = document.getElementsByClassName('bd-highlight')[0];
    var footer = document.getElementsByClassName('card-footer')[0];

    bkup_msg = msg_body.innerHTML;
    bkup_header = header.innerHTML;
    bkup_footer = footer.innerHTML;

    msg_body.innerHTML = document.getElementById("faq").innerHTML;
    //header.innerHTML = "";
    footer.innerHTML = document.getElementById("faqfooter").innerHTML;
    if (isFaqDisplayed === false)
    {
        var msg_body = document.getElementById("msg_container");
        var footer = document.getElementsByClassName('card-footer')[0];
        
        bkup_msg = msg_body.innerHTML;
        bkup_footer = footer.innerHTML;
        
        msg_body.innerHTML = document.getElementById("faq").innerHTML;
    
        footer.innerHTML = document.getElementById("faqfooter").innerHTML;

        document.getElementById("defaultOpen").click();
    
        isFaqDisplayed = true;
    }
}

function openFaq(evt, pageName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(pageName).style.display = "block";
    evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();
  
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        alert("yes");
        this.classList.toggle("accordionactive");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
        } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
        } 
    });
}

function confirmEnd() {

    if (isFaqDisplayed === true)
    {
        var msg_body = document.getElementById("msg_container");
        var footer = document.getElementsByClassName('card-footer')[0];

        msg_body.innerHTML = bkup_msg;
        footer.innerHTML = bkup_footer;
    }
    else if (isConfirmPageDisplayed === false)
    {
        var msg_body = document.getElementById("msg_container");

    document.getElementsByClassName('card-footer')[0].style.visibility = 'hidden';

    msg_body.innerHTML = "<div class='mid-center-div'><div><h5>Are you sure you want to end the conversation?</h5><div><button class='btn btn-secondary width-45' onclick='handleCancelEndConversation()'>No</button><button class='btn btn-primary width-45' onclick='handleEndConversation()'>Yes</button></div></div></div>";
        bkup_msg = msg_body.innerHTML;

        document.getElementsByClassName('card-footer')[0].style.visibility = 'hidden';
        
        msg_body.innerHTML = "<div class='mid-center-div'><div><h5>Are you sure you want to end the conversation?</h5><div><button class='btn btn-secondary width-45' onclick='handleCancelEndConversation()'>No</button><button class='btn btn-primary width-45' onclick='handleEndConversation()'>Yes</button></div></div></div>";

        isConfirmPageDisplayed = true;
    }

    isFaqDisplayed = false;
}

function handleEndConversation() {
    endChat();
    var msg_body = document.getElementById("msg_container");
    document.getElementsByClassName('card-footer')[0].style.visibility = 'visible';
    document.getElementsByClassName('card-footer')[0].innerHTML = "<div><button class='btn btn-primary btn-block' onClick='restartConvo()'>Start a new chat</button></div>";
    msg_body.innerHTML = "<div class='text-center'><h5>Chat session closed</h5><span>This chat session has ended.</span></div>";
}

function handleEndConversationTimeout() {
    var msg_body = document.getElementById("msg_container");
    document.getElementsByClassName('card-footer')[0].style.visibility = 'visible';
    document.getElementsByClassName('card-footer')[0].innerHTML = "<div><button class='btn btn-primary btn-block' onClick='restartConvo()'>Start a new chat</button></div>";
    msg_body.innerHTML = "<div class='text-center'><h5>Chat session closed</h5><span>This chat session has ended.</span></div>";
}

function restartConvo(){
    parent.removeData("guid_token");
    window.location.reload();
}
function handleCancelEndConversation() {

    var msg_body = document.getElementById("msg_container");

    document.getElementsByClassName('card-footer')[0].style.visibility = 'visible';

    msg_body.innerHTML = bkup_msg;

    isConfirmPageDisplayed = false;
}

function toggleformgroupdropdown() {

    var element = document.getElementById("mobileCountryDropdown");
    element.classList.toggle("show");
    element.childNodes[3].classList.toggle("show");
}

function handleCountryListSubmit(event) {
    event.preventDefault();
    const form = document.getElementById('countryForm');
    const affiliatex = serialize(form).split("=")[1].split('-');
    const affiliate = affiliatex[0];
    var lang = affiliatex[1];
    parent.custLang = lang;
    console.log(parent.custLang);
    const payload = {
        type: 'SET_LANGUAGE',
        data: {
            affiliate
        }
    }
    parent.onSend(payload);
    const countrySelect = document.getElementById("countrySelect");
    countrySelect.remove();
}

function handleButtonOptionClick(event) {
    payload.data.text = event.target.textContent;
    payload.data.status = "false";
    payload.data.custLang = parent.custLang
    parent.onSend(payload);
    const butcont = document.getElementById("buttoncont");
    butcont.remove();
    bind_user_msg(event.target.textContent);
    // initSession();

}

function handleListOptionClick(event) {
    payload.data.text = event.target.textContent;
    payload.data.status = "false";
    payload.data.custLang = parent.custLang
    parent.onSend(payload);
    const butcont = document.getElementById("optlist");
    butcont.remove();
    bind_user_msg(event.target.textContent);
    // initSession();

}

function initSession() {
    parent.onSend({
        type: "INIT_SESSION",
        data: {}
    });
}

function endChat() {
    parent.onSend({
        type: "END_CONVERSATION"
    });
}

function handlechange(event) {

    switch (event.target.type) {
        case "select-one":
            selectData = event.target.value;
            break;
        case "input":
            inputData = event.target.value;
            break;
        default:
            break;
    }

}

function send_msg() {

    var sendchat = document.getElementById("send_msg").value;
    if (sendchat === "" || sendchat === null) {
        return;
    }
    var msg_container = document.getElementById("msg_container");
    var msg_body = document.getElementById("msg_card_body");
    const new_chat = parent.component.userchatbubble.replace("{message}", sendchat).replace("{timestamp}", formatAMPM(new Date()));
    msg_body.insertAdjacentHTML("beforeend", new_chat);
    msg_container.scrollTop = msg_container.scrollHeight;
    payload.data.text = sendchat;
    payload.data.custLang = parent.custLang
    console.log(payload);
    parent.onSend(payload);
    document.getElementById("send_msg").value = "";
}

function bind_user_msg(message) {
    const new_chat = parent.component.userchatbubble.replace("{message}", message).replace("{timestamp}", formatAMPM(new Date()));
    var msg_container = document.getElementById("msg_container");
    var msg_body = document.getElementById("msg_card_body");
    msg_body.insertAdjacentHTML("beforeend", new_chat);
    msg_container.scrollTop = msg_container.scrollHeight;
}

function bind_user_msgDT(message,date) {
    const new_chat = parent.component.userchatbubble.replace("{message}", message).replace("{timestamp}", formatAMPM(new Date(date)));
    var msg_body = document.getElementById("msg_card_body");
    msg_body.insertAdjacentHTML("beforeend", new_chat);
    msg_container.scrollTop = msg_container.scrollHeight;
}

function rafiki_msg(mes) {
    var msg_container = document.getElementById("msg_container");
    var msg_body = document.getElementById("msg_card_body");
    const new_chat = parent.component.rafikichatbubble.replace("{message}", mes).replace("{timestamp}", formatAMPM(new Date()));
    msg_body.insertAdjacentHTML("beforeend", new_chat);
    msg_container.scrollTop = msg_container.scrollHeight;
}

function rafiki_list(list) {
    var msg_container = document.getElementById("msg_container");
    var msg_body = document.getElementById("msg_card_body");
    const new_chat = parent.component.buttoncontainer.replace("{lists}", list);
    msg_body.insertAdjacentHTML("beforeend", new_chat);
    msg_container.scrollTop = msg_container.scrollHeight;
}

function rafiki_buttons(buttons) {
    var msg_body = document.getElementById("msg_card_body");
    var butlist = "";
    for (var w = 0; w < buttons.length; w++) {
        butlist += parent.component.button.replace("{text}", buttons[w]);
    }
    const new_chat = parent.component.buttoncontainer.replace("{buttons}", butlist);
    msg_body.insertAdjacentHTML("beforeend", new_chat);
    msg_container.scrollTop = msg_container.scrollHeight;
}

function rafiki_options_list(list) {
    var msg_container = document.getElementById("msg_container");
    var msg_body = document.getElementById("msg_card_body");
    var butlist = "";
    for (var w = 0; w < list.length; w++) {
        butlist += "<li onclick='handleListOptionClick(event)'>" + list[w] + "</li>";
    }
    const new_chat = parent.component.optionlist.replace("{list}", butlist);
    msg_body.insertAdjacentHTML("beforeend", new_chat);
    msg_container.scrollTop = msg_container.scrollHeight;
}

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function submitOnEnter(event) {
    if (event.which === 13) {
        send_msg();
        event.preventDefault();
    }
}

function renderTextData(message){
    if(message.message){
        for (var e = 0; e < message.message.length; e++) {
            rafiki_msg(message.message[e]);
        }
    }
    if (message.data && message.data.text) {
        for (var e = 0; e < message.data.text.length; e++) {
            rafiki_msg(message.data.text[e]);
        }
    }
}

function renderMessage(message) {

    if(message.type === 'SES_LOADED'){
        handleSessionLoaded(message.data.history);
    }

    if (message.type === 'TIMEOUT') {
        handleEndConversationTimeout();
    }

    if (message.type === 'AUTHENTICATED') {
        storeData('guid_token', message.data.guid_token);
    }

    if (message.type === 'SET_LANGUAGE') {
        // Show set_language component
        msg_card_body.innerHTML = "";
        msg_card_body.insertAdjacentHTML('beforeend', component.selectlist.replace("{lists}", affiliateoptionlang));
    }

    if (message.type === 'SHOW_IS_TYPING') {
        document.getElementById("status").innerText = message.data.message;
        var msg_container = document.getElementById("msg_container");
        var msg_body = document.getElementById("msg_card_body");
        const new_chat = parent.component.istyping;
        msg_body.insertAdjacentHTML("beforeend", new_chat);
        msg_container.scrollTop = msg_container.scrollHeight;
    }
    if (message.type === 'HIDE_IS_TYPING') {
        document.getElementById("status").innerText = "Typically replies in a few seconds";
        var istype = document.getElementById("istyping");
        istype.remove();
    }
    if(message.message){
        for (var e = 0; e < message.message.length; e++) {
            rafiki_msg(message.message[e]);
        }
    }
    if (message.data && message.data.text) {
        for (var e = 0; e < message.data.text.length; e++) {
            rafiki_msg(message.data.text[e]);
        }
    }
    if (message.has_suggestions) {
        switch (message.suggestion_list_type) {
            case 'buttons':
                rafiki_buttons(message.suggestion_list);
                break;
            case 'list':
                rafiki_options_list(message.suggestion_list);
                break;
            default:

        }
    }

    if (message.has_form) {
        switch (message.form_type) {
            case 'ecoBankClinetForm':
                clientform();
                clientform.init();
                break;
            case 'prehandover_form':
                prehandform();
                prehandform.init();
                break;
            case 'nonEcoBankClinetForm':
                prehandform();
                prehandform.init();
                break;
            case 'mobile_money_form':
                mobilemoneyform();
                mobilemoneyform.init();
                break;
            case 'OTP':
                submitotpform();
                submitotpform.init();
                break;
            default:

        }

    }
}

document.getElementById("send_msg").addEventListener("keypress", submitOnEnter);

// $(document).ready(function () {

window.addEventListener('load', () => {
    // 1st, we set the correct status when the page loads
    navigator.onLine ? '' : parent.showNoticeAlert('Your device is not connected to the internet');

    // now we listen for network status changes
    window.addEventListener('online', () => {
        parent.showNoticeSuccess("We're back online");
        parent.fetchAffiliate();
        initSocketCon();
    });

    window.addEventListener('offline', () => {
        parent.showNoticeAlert('Your Device is not connected to the internet');
    });
});

if (navigator.onLine) {
    initSocketCon();
}

function initSocketCon() {
    webSocket = new WebSocket('wss://ice.ecobank.com/socketv4/websocket');
    webSocket.onerror = function (event) {
        parent.onError(event);
    };

    webSocket.onopen = function (event) {
        console.log('Opening')
        console.log(new Date());
        parent.onOpen(event);
    };

    webSocket.onmessage = function (event) {
        parent.onMessage(event);
    };
    webSocket.onclose = function (event) {
        parent.onClose(event);
    }

    parent.onSend = function (data) {
        if (webSocket.readyState === WebSocket.CLOSED) {
            initSocketCon();
        } else if (webSocket.readyState === WebSocket.CONNECTING) {
            setTimeout(function () {
                webSocket.send(JSON.stringify(data));
            }, 5000);
        } else if (webSocket.readyState === WebSocket.CLOSING) {
            initSocketCon();
        } else {
            webSocket.send(JSON.stringify(data));
        }
    }

    parent.onOpen = function (event) {

        if (parent.retrieveData("guid_token") != undefined) {
            var det = {
                token: parent.retrieveData("guid_token")
            };
            onSend({
                type: 'AUTH',
                data: det
            });
        } else {
            onSend({
                type: 'AUTH',
                data: {}
            });
        }


    }

    parent.onClose = function (event) {
        console.log('socket connection closed');
        console.log(new Date());
    }

    parent.onError = function (event) {
        console.log(event);
    }

    parent.onMessage = function (event) {
        var retObj = JSON.parse(event.data);
        console.log(retObj);
        renderMessage(retObj);
    }


}

// });


function storeData(key, value) {
    localStorage.setItem(key, value);
}

function retrieveData(key) {
    return localStorage.getItem(key);
}

function removeData(key) {
    localStorage.removeItem(key);
}

/* function handleFormSubmitData(event) {
    // Triggerred onclick
    event.preventDefault();
    console.log(event);
    console.log(selectData);

    // onSend(data);
} */

function showNoticeAlert(text) {
    document.getElementById("alert-panel-success").classList.add("doHide");
    var el = document.getElementById("alert-panel").childNodes;
    el[1].innerText = text;
    document.getElementById("alert-panel").classList.remove("doHide");
    var olst = document.getElementById("olst");
    olst.classList.remove("online_icon");
    olst.classList.add("offline_icon");

}

function showNoticeSuccess(text) {
    document.getElementById("alert-panel").classList.add("doHide");
    var el = document.getElementById("alert-panel-success").childNodes;
    el[1].innerText = text;
    document.getElementById("alert-panel-success").classList.remove("doHide");
    setTimeout(function () {
        hideNoticeSuccess();
    }, 3000);
    // change class here 
    var olst = document.getElementById("olst");
    olst.classList.remove("offline_icon");
    olst.classList.add("online_icon");
}

function hideNoticeSuccess() {
    var el = document.getElementById("alert-panel-success");
    el.classList.add("doHide");
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function doAjaxRequestPOST(url, method, headers, data, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    if (headers.length > 0) {
        for (var t = 0; t < headers.length; t++) {
            xhr.setRequestHeader(headers[t]['key'], headers[t][' value']);
        }
    }
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            success(xhr);
        } else {
            error(xhr);
        }
    };
    xhr.send(data);

}

function doAjaxRequestGET(url, method, headers, success, error) {
    var xhr = new XMLHttpRequest();
    xhr.open(method, url, true);
    if (headers.length > 0) {
        for (var t = 0; t < headers.length; t++) {
            xhr.setRequestHeader(headers[t]['key'], headers[t][' value']);
        }
    }
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            success(xhr.response);
        } else {
            error(xhr);
        }
    };
    xhr.send();

}

function fetchAffiliateLang() {
    doAjaxRequestGET("https://rafikiv5.eu-gb.mybluemix.net/proxy/getawaaffiliates", "GET", [{
            "key": "Accept",
            "value": "application/json"
        }],
        function success(data) {
            affiliatelist = JSON.parse(data);
            for (var x = 0; x < affiliatelist.length; x++) {
                affiliateoptionlang += "<option value='" + affiliatelist[x].affiliateCode+ "-"+affiliatelist[x].language+ "'>" + affiliatelist[x].affiliateName + "</option>";
            }
        },
        function error(error) {
            console.log(error);
        });
}



function fetchAffiliate() {
    doAjaxRequestGET("https://rafikiv5.eu-gb.mybluemix.net/proxy/getawaaffiliates", "GET", [{
            "key": "Accept",
            "value": "application/json"
        }],
        function success(data) {
            affiliatelist = JSON.parse(data);
            for (var x = 0; x < affiliatelist.length; x++) {
                affiliateoption += "<option value='" + affiliatelist[x].affiliateCode+"'>" + affiliatelist[x].affiliateName + "</option>";
            }
        },
        function error(error) {
            console.log(error);
        });
}

function initClientForm() {
    var clientform = this;
    var accountnumber = "";
    var country = "";
    var steps = [];
    var stepsvalue = [];
    var container = "<div class='form-div' id='clientcontainer'>{current_step}</div>";
    var currentstep = 0;
    steps.push("<div id='curstep'><div>Account Number</div><div><input type='text' placeholder='Account Number' required maxlength='16' name='acctNum' class='form-control' id='optval' value=''/></div><div><input type='submit' class='btn btn-primary btn-sm' value='Next' onclick='clientform.next()' /></div></div>");
    steps.push("<div id='curstep'><div>Country</div><div><select class='form-control' required id='optval'><option value=''>Select country where you are domicilled</option>" + parent.affiliateoption + "</select></div><div><input type='button' class='btn btn-secondary btn-sm' value='Previous' onclick='clientform.previous()' /> <input type='button' class='btn btn-primary btn-sm' value='Next' onclick='clientform.next()' /></div></div>");
    steps.push("<div class='form-div' id='curstep'><div>Please Confirm your information before submitting</div><div><ul><li><h5>ACCOUNT NUMBER</h5><span>{acct_number}</span></li><li><h5>AFFILIATE</h5><span>{aff}</span></li></ul></div><div><button class='btn btn-secondary btn-sm' onclick='clientform.previous()'>Back</button> <button class='btn btn-primary btn-sm' onclick='clientform.complete()'>Submit</button></div></div>");
    stepsvalue.push("");
    stepsvalue.push("");

    function init() {
        container = container.replace('{current_step}', steps[currentstep]);
        var msg_container = document.getElementById("msg_container");
        var msg_body = document.getElementById("msg_card_body");
        msg_body.insertAdjacentHTML("beforeend", container);
        msg_container.scrollTop = msg_container.scrollHeight;
    }

    function next() {
        var dVal = document.getElementById("optval");
        stepsvalue[currentstep] = dVal.value;
        var prev = document.getElementById("curstep");
        prev.remove();
        currentstep += 1;
        var cont = document.getElementById("clientcontainer");
        if (currentstep == 2) {
            cont.insertAdjacentHTML("beforeend", steps[currentstep].replace('{acct_number}', stepsvalue[0]).replace('{aff}', stepsvalue[1]));
        } else {
            cont.insertAdjacentHTML("beforeend", steps[currentstep]);
            var dVal = document.getElementById("optval");
            dVal.value = stepsvalue[currentstep];
        }

    }

    function previous() {
        var prev = document.getElementById("curstep");
        prev.remove();
        currentstep -= 1;
        var cont = document.getElementById("clientcontainer");
        cont.insertAdjacentHTML("beforeend", steps[currentstep]);
        var dVal = document.getElementById("optval");
        dVal.value = stepsvalue[currentstep];
    }

    function complete() {
        // initSession();
        var data = {
            type: "VALIDATION_FORM",
            data: {
                AFFILIATE_CODE: stepsvalue[1],
                ACCOUNT_NUMBER: stepsvalue[0],
                IS_CUSTOMER: true
            }
        }
        parent.onSend(data);
        var el = document.getElementById("clientcontainer");
        el.remove();
    }
    initClientForm.init = init;
    initClientForm.next = next;
    initClientForm.previous = previous;
    initClientForm.complete = complete;

}

function preHandOverForm() {
    var component = "<div class='form-div' id ='handoverform'><form><div class='row'><div class='col-md-12'>Full Name</div><div class='col-md-12'><input class='form-control' type='text' placeholder='Full Name' id='handfullname'/></div></div><div class='row'><div class='col-md-12'>Mobile Number</div><div class='col-md-12'><div class='input-group mb-3'><select class='form-control' id='handnumberprefix' required><option data-countrycode='DZ' value='213'>Algeria(+213)</option><option data-countrycode='AD' value='376'>Andorra(+376)</option><option data-countrycode='AO' value='244'>Angola(+244)</option><option data-countrycode='AI' value='1264'>Anguilla(+1264)</option><option data-countrycode='AG' value='1268'>Antigua &amp; Barbuda(+1268)</option><option data-countrycode='AR' value='54'>Argentina(+54)</option><option data-countrycode='AM' value='374'>Armenia(+374)</option><option data-countrycode='AW' value='297'>Aruba(+297)</option><option data-countrycode='AU' value='61'>Australia(+61)</option><option data-countrycode='AT' value='43'>Austria(+43)</option><option data-countrycode='AZ' value='994'>Azerbaijan(+994)</option><option data-countrycode='BS' value='1242'>Bahamas(+1242)</option><option data-countrycode='BH' value='973'>Bahrain(+973)</option><option data-countrycode='BD' value='880'>Bangladesh(+880)</option><option data-countrycode='BB' value='1246'>Barbados(+1246)</option><option data-countrycode='BY' value='375'>Belarus(+375)</option><option data-countrycode='BE' value='32'>Belgium(+32)</option><option data-countrycode='BZ' value='501'>Belize(+501)</option><option data-countrycode='BJ' value='229'>Benin(+229)</option><option data-countrycode='BM' value='1441'>Bermuda(+1441)</option><option data-countrycode='BT' value='975'>Bhutan(+975)</option><option data-countrycode='BO' value='591'>Bolivia(+591)</option><option data-countrycode='BA' value='387'>Bosnia Herzegovina(+387)</option><option data-countrycode='BW' value='267'>Botswana(+267)</option>" +
        "<option data-countrycode='BR' value='55'>Brazil(+55)</option><option data-countrycode='BN' value='673'>Brunei(+673)</option><option data-countrycode='BG' value='359'>Bulgaria(+359)</option><option data-countrycode='BF' value='226'>BurkinaFaso(+226)</option><option data-countrycode='BI' value='257'>Burundi(+257)</option><option data-countrycode='KH' value='855'>Cambodia(+855)</option><option data-countrycode='CM' value='237'>Cameroon(+237)</option><option data-countrycode='CA' value='1'>Canada(+1)</option><option data-countrycode='CV' value='238'>CapeVerdeIslands(+238)</option><option data-countrycode='KY' value='1345'>CaymanIslands(+1345)</option><option data-countrycode='CF' value='236'>CentralAfricanRepublic(+236)</option><option data-countrycode='CL' value='56'>Chile(+56)</option><option data-countrycode='CN' value='86'>China(+86)</option><option data-countrycode='CO' value='57'>Colombia(+57)</option><option data-countrycode='KM' value='269'>Comoros(+269)</option><option data-countrycode='CG' value='242'>Congo(+242)</option><option data-countrycode='CK' value='682'>CookIslands(+682)</option><option data-countrycode='CR' value='506'>CostaRica(+506)</option><option data-countrycode='HR' value='385'>Croatia(+385)</option><option data-countrycode='CU' value='53'>Cuba(+53)</option><option data-countrycode='CY' value='90392'>CyprusNorth(+90392)</option><option data-countrycode='CY' value='357'>CyprusSouth(+357)</option><option data-countrycode='CZ' value='42'>CzechRepublic(+42)</option><option data-countrycode='DK' value='45'>Denmark(+45)</option><option data-countrycode='DJ' value='253'>Djibouti(+253)</option><option data-countrycode='DM' value='1809'>Dominica(+1809)</option><option data-countrycode='DO' value='1809'>DominicanRepublic(+1809)</option><option data-countrycode='EC' value='593'>Ecuador(+593)</option><option data-countrycode='EG' value='20'>Egypt(+20)</option>" +
        "<option data-countrycode='SV' value='503'>ElS alvador(+503)</option><option data-countrycode='GQ' value='240'>Equatorial Guinea(+240)</option><option data-countrycode='ER' value='291'>Eritrea(+291)</option><option data-countrycode='EE' value='372'>Estonia(+372)</option><option data-countrycode='ET' value='251'>Ethiopia(+251)</option><option data-countrycode='FK' value='500'>FalklandIslands(+500)</option><option data-countrycode='FO' value='298'>FaroeIslands(+298)</option><option data-countrycode='FJ' value='679'>Fiji(+679)</option><option data-countrycode='FI' value='358'>Finland(+358)</option><option data-countrycode='FR' value='33'>France(+33)</option><option data-countrycode='GF' value='594'>FrenchGuiana(+594)</option>" +
        "<option data-countrycode='PF' value='689'>French Polynesia(+689)</option><option data-countrycode='GA' value='241'>Gabon(+241)</option><option data-countrycode='GM' value='220'>Gambia(+220)</option><option data-countrycode='GE' value='7880'>Georgia(+7880)</option><option data-countrycode='DE' value='49'>Germany(+49)</option><option data-countrycode='GH' value='233'>Ghana(+233)</option><option data-countrycode='GI' value='350'>Gibraltar(+350)</option><option data-countrycode='GR' value='30'>Greece(+30)</option><option data-countrycode='GL' value='299'>Greenland(+299)</option><option data-countrycode='GD' value='1473'>Grenada(+1473)</option><option data-countrycode='GP' value='590'>Guadeloupe(+590)</option><option data-countrycode='GU' value='671'>Guam(+671)</option><option data-countrycode='GT' value='502'>Guatemala(+502)</option><option data-countrycode='GN' value='224'>Guinea(+224)</option><option data-countrycode='GW' value='245'>Guinea-Bissau(+245)</option><option data-countrycode='GY' value='592'>Guyana(+592)</option><option data-countrycode='HT' value='509'>Haiti(+509)</option><option data-countrycode='HN' value='504'>Honduras(+504)</option><option data-countrycode='HK' value='852'>HongKong(+852)</option><option data-countrycode='HU' value='36'>Hungary(+36)</option><option data-countrycode='IS' value='354'>Iceland(+354)</option><option data-countrycode='IN' value='91'>India(+91)</option><option data-countrycode='ID' value='62'>Indonesia(+62)</option><option data-countrycode='IR' value='98'>Iran(+98)</option><option data-countrycode='IQ' value='964'>Iraq(+964)</option><option data-countrycode='IE' value='353'>Ireland(+353)</option><option data-countrycode='IL' value='972'>Israel(+972)</option><option data-countrycode='IT' value='39'>Italy(+39)</option><option data-countrycode='JM' value='1876'>Jamaica(+1876)</option><option data-countrycode='JP' value='81'>Japan(+81)</option><option data-countrycode='JO' value='962'>Jordan(+962)</option>" +
        "<option data-countrycode='KZ' value='7'>Kazakhstan(+7)</option><option data-countrycode='KE' value='254'>Kenya(+254)</option><option data-countrycode='KI' value='686'>Kiribati(+686)</option><option data-countrycode='KP' value='850'>KoreaNorth(+850)</option><option data-countrycode='KR' value='82'>KoreaSouth(+82)</option><option data-countrycode='KW' value='965'>Kuwait(+965)</option><option data-countrycode='KG' value='996'>Kyrgyzstan(+996)</option><option data-countrycode='LA' value='856'>Laos(+856)</option><option data-countrycode='LV' value='371'>Latvia(+371)</option><option data-countrycode='LB' value='961'>Lebanon(+961)</option><option data-countrycode='LS' value='266'>Lesotho(+266)</option><option data-countrycode='LR' value='231'>Liberia(+231)</option><option data-countrycode='LY' value='218'>Libya(+218)</option><option data-countrycode='LI' value='417'>Liechtenstein(+417)</option><option data-countrycode='LT' value='370'>Lithuania(+370)</option><option data-countrycode='LU' value='352'>Luxembourg(+352)</option><option data-countrycode='MO' value='853'>Macao(+853)</option><option data-countrycode='MK' value='389'>Macedonia(+389)</option><option data-countrycode='MG' value='261'>Madagascar(+261)</option><option data-countrycode='MW' value='265'>Malawi(+265)</option><option data-countrycode='MY' value='60'>Malaysia(+60)</option><option data-countrycode='MV' value='960'>Maldives(+960)</option><option data-countrycode='ML' value='223'>Mali(+223)</option><option data-countrycode='MT' value='356'>Malta(+356)</option><option data-countrycode='MH' value='692'>MarshallIslands(+692)</option><option data-countrycode='MQ' value='596'>Martinique(+596)</option><option data-countrycode='MR' value='222'>Mauritania(+222)</option><option data-countrycode='YT' value='269'>Mayotte(+269)</option><option data-countrycode='MX' value='52'>Mexico(+52)</option><option data-countrycode='FM' value='691'>Micronesia(+691)</option><option data-countrycode='MD' value='373'>Moldova(+373)</option>" +
        "<option data-countrycode='MC' value='377'>Monaco(+377)</option><option data-countrycode='MN' value='976'>Mongolia(+976)</option><option data-countrycode='MS' value='1664'>Montserrat(+1664)</option><option data-countrycode='MA' value='212'>Morocco(+212)</option><option data-countrycode='MZ' value='258'>Mozambique(+258)</option><option data-countrycode='MN' value='95'>Myanmar(+95)</option><option data-countrycode='NA' value='264'>Namibia(+264)</option><option data-countrycode='NR' value='674'>Nauru(+674)</option><option data-countrycode='NP' value='977'>Nepal(+977)</option>" +
        "<option data-countrycode='NL' value='31'>Netherlands(+31)</option><option data-countrycode='NC' value='687'> aledonia(+687)</option><option data-countrycode='NZ' value='64'>New Zealand(+64)</option><option data-countrycode='NI' value='505'>Nicaragua(+505)</option><option data-countrycode='NE' value='227'>Niger(+227)</option><option data-countrycode='NG' value='234'>Nigeria(+234)</option><option data-countrycode='NU' value='683'>Niue(+683)</option><option data-countrycode='NF' value='672'>NorfolkIslands(+672)</option><option data-countrycode='NP' value='670'>NorthernMarianas(+670)</option><option data-countrycode='NO' value='47'>Norway(+47)</option><option data-countrycode='OM' value='968'>Oman(+968)</option><option data-countrycode='PW' value='680'>Palau(+680)</option><option data-countrycode='PA' value='507'>Panama(+507)</option><option data-countrycode='PG' value='675'>PapuaNewGuinea(+675)</option><option data-countrycode='PY' value='595'>Paraguay(+595)</option><option data-countrycode='PE' value='51'>Peru(+51)</option><option data-countrycode='PH' value='63'>Philippines(+63)</option><option data-countrycode='PL' value='48'>Poland(+48)</option>" +
        "<option data-countrycode='PT' value='351'>Portugal(+351)</option><option data-countrycode='PR' value='1787'>Puerto Rico(+1787)</option><option data-countrycode='QA' value='974'>Qatar(+974)</option><option data-countrycode='RE' value='262'>Reunion(+262)</option><option data-countrycode='RO' value='40'>Romania(+40)</option><option data-countrycode='RU' value='7'>Russia(+7)</option><option data-countrycode='RW' value='250'>Rwanda(+250)</option><option data-countrycode='SM' value='378'>San Marino(+378)</option><option data-countrycode='ST' value='239'>Sao Tome &amp; Principe(+239)</option><option data-countrycode='SA' value='966'>Saudi Arabia(+966)</option><option data-countrycode='SN' value='221'>Senegal(+221)</option><option data-countrycode='CS' value='381'>Serbia(+381)</option><option data-countrycode='SC' value='248'>Seychelles(+248)</option><option data-countrycode='SL' value='232'>Sierra Leone(+232)</option><option data-countrycode='SG' value='65'>Singapore(+65)</option><option data-countrycode='SK' value='421'>SlovakRepublic(+421)</option><option data-countrycode='SI' value='386'>Slovenia(+386)</option><option data-countrycode='SB' value='677'>Solomon Islands(+677)</option><option data-countrycode='SO' value='252'>Somalia(+252)</option><option data-countrycode='ZA' value='27'>South Africa(+27)</option><option data-countrycode='ES' value='34'>Spain(+34)</option><option data-countrycode='LK' value='94'>Sri Lanka(+94)</option><option data-countrycode='SH' value='290'>St. Helena(+290)</option><option data-countrycode='KN' value='1869'>St. Kitts(+1869)</option><option data-countrycode='SC' value='1758'>St. Lucia(+1758)</option><option data-countrycode='SD' value='249'>Sudan(+249)</option><option data-countrycode='SR' value='597'>Suriname(+597)</option><option data-countrycode='SZ' value='268'>Swaziland(+268)</option><option data-countrycode='SE' value='46'>Sweden(+46)</option><option data-countrycode='CH' value='41'>Switzerland(+41)</option><option data-countrycode='SI' value='963'>Syria(+963)</option>" +
        "<option data-countrycode='TW' value='886'>Taiwan(+886)</option><option data-countrycode='TJ' value='7'>Tajikstan(+7)</option><option data-countrycode='TH' value='66'>Thailand(+66)</option><option data-countrycode='TG' value='228'>Togo(+228)</option><option data-countrycode='TO' value='676'>Tonga(+676)</option><option data-countrycode='TT' value='1868'>Trinidad&amp;Tobago(+1868)</option><option data-countrycode='TN' value='216'>Tunisia(+216)</option><option data-countrycode='TR' value='90'>Turkey(+90)</option><option data-countrycode='TM' value='7'>Turkmenistan(+7)</option><option data-countrycode='TM' value='993'>Turkmenistan(+993)</option><option data-countrycode='TC' value='1649'>Turks&amp;CaicosIslands(+1649)</option><option data-countrycode='TV' value='688'>Tuvalu(+688)</option><option data-countrycode='UG' value='256'>Uganda(+256)</option><option data-countrycode='GB' value='44'>UK(+44)</option><option data-countrycode='UA' value='380'>Ukraine(+380)</option><option data-countrycode='AE' value='971'>UnitedArabEmirates(+971)</option><option data-countrycode='UY' value='598'>Uruguay(+598)</option><option data-countrycode='US' value='1'>USA(+1)</option><option data-countrycode='UZ' value='7'>Uzbekistan(+7)</option><option data-countrycode='VU' value='678'>Vanuatu(+678)</option><option data-countrycode='VA' value='379'>VaticanCity(+379)</option><option data-countrycode='VE' value='58'>Venezuela(+58)</option><option data-countrycode='VN' value='84'>Vietnam(+84)</option><option data-countrycode='VG' value='84'>VirginIslands-British(+1284)</option><option data-countrycode='VI' value='84'>Virgin Islands-US(+1340)</option><option data-countrycode='WF' value='681'>Wallis &amp; Futuna(+681)</option><option data-countrycode='YE' value='969'>Yemen(North)(+969)</option><option data-countrycode='YE' value='967'>Yemen(South)(+967)</option><option data-countrycode='ZM' value='260'>Zambia(+260)</option><option data-countrycode='ZW' value='263'>Zimbabwe(+263)</option></select><input type='text'required class='form-control width-30' id ='handnumber'></div></div></div>" +
        "<div class='row'><div class='col-md-12'>Country</div><div class='col-md-12'><select class='form-control' required id ='handcountry'><option value=''>Country</option>" + affiliateoption + "</select></div></div><div class='row'><div><input type='submit' class='btn btn-primary btn-sm' value='Submit' onclick='prehandform.doTransfer(event)'/></div></div></form></div>";

    function init() {
        var msg_container = document.getElementById("msg_container");
        var msg_body = document.getElementById("msg_card_body");
        msg_body.insertAdjacentHTML("beforeend", component);
        msg_container.scrollTop = msg_container.scrollHeight;
    }

    function doTransfer(event) {
        event.preventDefault();
        var fullname = document.getElementById('handfullname').value;
        var numberprefix = document.getElementById('handnumberprefix').value;
        var handnumber = document.getElementById('handnumber').value;
        var country = document.getElementById('handcountry').value;

        // {"type":"VALIDATION_FORM","data":{"AFFILIATE_CODE":"ENG",
        // "ACCOUNT_NAME":null,"ACCOUNT_NUMBER":null,"PHONE":"+23407062725721","SUMMARY":null,"NAME":"Gaffar Kasumu","IS_CUSTOMER":false}}"
        const data = {
            type: "VALIDATION_FORM",
            data: {
                AFFILIATE_CODE: country,
                ACCOUNT_NAME: "",
                ACCOUNT_NUMBER: "",
                PHONE: numberprefix + handnumber,
                SUMMARY: "",
                NAME: fullname,
                IS_CUSTOMER: false
            }
        }
        parent.onSend(data);
        var fo = document.getElementById("handoverform");
        fo.remove();
    }

    preHandOverForm.init = init;
    preHandOverForm.doTransfer = doTransfer;
}


function mobileMoneyForm() {
    var component = "<div class='form-div' id='mmform'><form><div class='row'><div class='col-md-12'>Your Registered Mobile App Number</div><div class='col-md-12'><div class='input-group mb-3'><select class='form-control' required id='mobileformyournumbercode'><option data-countryCode='DZ' value='213'>Algeria (+213)</option><option data-countryCode='AD' value='376'>Andorra (+376)</option><option data-countryCode='AO' value='244'>Angola (+244)</option><option data-countryCode='AI' value='1264'>Anguilla (+1264)</option><option data-countryCode='AG' value='1268'>Antigua &amp; Barbuda (+1268)</option><option data-countryCode='AR' value='54'>Argentina (+54)</option><option data-countryCode='AM' value='374'>Armenia (+374)</option><option data-countryCode='AW' value='297'>Aruba (+297)</option><option data-countryCode='AU' value='61'>Australia (+61)</option><option data-countryCode='AT' value='43'>Austria (+43)</option> <option data-countryCode='AZ' value='994'>Azerbaijan (+994)</option> <option data-countryCode='BS' value='1242'>Bahamas (+1242)</option> <option data-countryCode='BH' value='973'>Bahrain (+973)</option> <option data-countryCode='BD' value='880'>Bangladesh (+880)</option> <option data-countryCode='BB' value='1246'>Barbados (+1246)</option> <option data-countryCode='BY' value='375'>Belarus (+375)</option> <option data-countryCode='BE' value='32'>Belgium (+32)</option> <option data-countryCode='BZ' value='501'>Belize (+501)</option> <option data-countryCode='BJ' value='229'>Benin (+229)</option> <option data-countryCode='BM' value='1441'>Bermuda (+1441)</option> <option data-countryCode='BT' value='975'>Bhutan (+975)</option> <option data-countryCode='BO' value='591'>Bolivia (+591)</option> <option data-countryCode='BA' value='387'>Bosnia Herzegovina (+387) </option> <option data-countryCode='BW' value='267'>Botswana (+267)</option> <option data-countryCode='BR' value='55'>Brazil (+55)</option> <option data-countryCode='BN' value='673'>Brunei (+673)</option> <option data-countryCode='BG' value='359'>Bulgaria (+359)</option> <option data-countryCode='BF' value='226'>Burkina Faso (+226)</option> <option data-countryCode='BI' value='257'>Burundi (+257)</option> <option data-countryCode='KH' value='855'>Cambodia (+855)</option> <option data-countryCode='CM' value='237'>Cameroon (+237)</option> <option data-countryCode='CA' value='1'>Canada (+1)</option> <option data-countryCode='CV' value='238'>Cape Verde Islands (+238) </option> <option data-countryCode='KY' value='1345'>Cayman Islands (+1345) </option> <option data-countryCode='CF' value='236'>Central African Republic     (+236)</option> <option data-countryCode='CL' value='56'>Chile (+56)</option> <option data-countryCode='CN' value='86'>China (+86)</option> <option data-countryCode='CO' value='57'>Colombia (+57)</option> <option data-countryCode='KM' value='269'>Comoros (+269)</option> <option data-countryCode='CG' value='242'>Congo (+242)</option> <option data-countryCode='CK' value='682'>Cook Islands (+682)</option> <option data-countryCode='CR' value='506'>Costa Rica (+506)</option> <option data-countryCode='HR' value='385'>Croatia (+385)</option> <option data-countryCode='CU' value='53'>Cuba (+53)</option> <option data-countryCode='CY' value='90392'>Cyprus North (+90392) </option> <option data-countryCode='CY' value='357'>Cyprus South (+357)</option> <option data-countryCode='CZ' value='42'>Czech Republic (+42)</option> <option data-countryCode='DK' value='45'>Denmark (+45)</option> <option data-countryCode='DJ' value='253'>Djibouti (+253)</option> <option data-countryCode='DM' value='1809'>Dominica (+1809)</option> <option data-countryCode='DO' value='1809'>Dominican Republic (+1809) </option> <option data-countryCode='EC' value='593'>Ecuador (+593)</option> <option data-countryCode='EG' value='20'>Egypt (+20)</option> <option data-countryCode='SV' value='503'>El Salvador (+503)</option> <option data-countryCode='GQ' value='240'>Equatorial Guinea (+240) </option> <option data-countryCode='ER' value='291'>Eritrea (+291)</option> <option data-countryCode='EE' value='372'>Estonia (+372)</option> <option data-countryCode='ET' value='251'>Ethiopia (+251)</option> <option data-countryCode='FK' value='500'>Falkland Islands (+500) </option> <option data-countryCode='FO' value='298'>Faroe Islands (+298)</option> <option data-countryCode='FJ' value='679'>Fiji (+679)</option> <option data-countryCode='FI' value='358'>Finland (+358)</option> <option data-countryCode='FR' value='33'>France (+33)</option> <option data-countryCode='GF' value='594'>French Guiana (+594)</option> <option data-countryCode='PF' value='689'>French Polynesia (+689) </option> <option data-countryCode='GA' value='241'>Gabon (+241)</option> <option data-countryCode='GM' value='220'>Gambia (+220)</option> <option data-countryCode='GE' value='7880'>Georgia (+7880)</option> <option data-countryCode='DE' value='49'>Germany (+49)</option> <option data-countryCode='GH' value='233'>Ghana (+233)</option> <option data-countryCode='GI' value='350'>Gibraltar (+350)</option> <option data-countryCode='GR' value='30'>Greece (+30)</option> <option data-countryCode='GL' value='299'>Greenland (+299)</option> <option data-countryCode='GD' value='1473'>Grenada (+1473)</option> <option data-countryCode='GP' value='590'>Guadeloupe (+590)</option> <option data-countryCode='GU' value='671'>Guam (+671)</option> <option data-countryCode='GT' value='502'>Guatemala (+502)</option> <option data-countryCode='GN' value='224'>Guinea (+224)</option> <option data-countryCode='GW' value='245'>Guinea - Bissau (+245) </option> <option data-countryCode='GY' value='592'>Guyana (+592)</option> <option data-countryCode='HT' value='509'>Haiti (+509)</option> <option data-countryCode='HN' value='504'>Honduras (+504)</option> <option data-countryCode='HK' value='852'>Hong Kong (+852)</option>" +
        "<option data-countryCode='HU' value='36'>Hungary (+36)</option> <option data-countryCode='IS' value='354'>Iceland (+354)</option> <option data-countryCode='IN' value='91'>India (+91)</option> <option data-countryCode='ID' value='62'>Indonesia (+62)</option> <option data-countryCode='IR' value='98'>Iran (+98)</option> <option data-countryCode='IQ' value='964'>Iraq (+964)</option> <option data-countryCode='IE' value='353'>Ireland (+353)</option> <option data-countryCode='IL' value='972'>Israel (+972)</option> <option data-countryCode='IT' value='39'>Italy (+39)</option> <option data-countryCode='JM' value='1876'>Jamaica (+1876)</option> <option data-countryCode='JP' value='81'>Japan (+81)</option> <option data-countryCode='JO' value='962'>Jordan (+962)</option> <option data-countryCode='KZ' value='7'>Kazakhstan (+7)</option> <option data-countryCode='KE' value='254'>Kenya (+254)</option> <option data-countryCode='KI' value='686'>Kiribati (+686)</option> <option data-countryCode='KP' value='850'>Korea North (+850)</option> <option data-countryCode='KR' value='82'>Korea South (+82)</option> <option data-countryCode='KW' value='965'>Kuwait (+965)</option> <option data-countryCode='KG' value='996'>Kyrgyzstan (+996)</option> <option data-countryCode='LA' value='856'>Laos (+856)</option> <option data-countryCode='LV' value='371'>Latvia (+371)</option> <option data-countryCode='LB' value='961'>Lebanon (+961)</option> <option data-countryCode='LS' value='266'>Lesotho (+266)</option> <option data-countryCode='LR' value='231'>Liberia (+231)</option> <option data-countryCode='LY' value='218'>Libya (+218)</option> <option data-countryCode='LI' value='417'>Liechtenstein (+417)</option> <option data-countryCode='LT' value='370'>Lithuania (+370)</option> <option data-countryCode='LU' value='352'>Luxembourg (+352)</option> <option data-countryCode='MO' value='853'>Macao (+853)</option> <option data-countryCode='MK' value='389'>Macedonia (+389)</option> <option data-countryCode='MG' value='261'>Madagascar (+261)</option> <option data-countryCode='MW' value='265'>Malawi (+265)</option> <option data-countryCode='MY' value='60'>Malaysia (+60)</option> <option data-countryCode='MV' value='960'>Maldives (+960)</option> <option data-countryCode='ML' value='223'>Mali (+223)</option> <option data-countryCode='MT' value='356'>Malta (+356)</option> <option data-countryCode='MH' value='692'>Marshall Islands (+692) </option> <option data-countryCode='MQ' value='596'>Martinique (+596)</option> <option data-countryCode='MR' value='222'>Mauritania (+222)</option> <option data-countryCode='YT' value='269'>Mayotte (+269)</option> <option data-countryCode='MX' value='52'>Mexico (+52)</option> <option data-countryCode='FM' value='691'>Micronesia (+691)</option> <option data-countryCode='MD' value='373'>Moldova (+373)</option> <option data-countryCode='MC' value='377'>Monaco (+377)</option> <option data-countryCode='MN' value='976'>Mongolia (+976)</option> <option data-countryCode='MS' value='1664'>Montserrat (+1664)</option> <option data-countryCode='MA' value='212'>Morocco (+212)</option> <option data-countryCode='MZ' value='258'>Mozambique (+258)</option> <option data-countryCode='MN' value='95'>Myanmar (+95)</option> <option data-countryCode='NA' value='264'>Namibia (+264)</option> <option data-countryCode='NR' value='674'>Nauru (+674)</option> <option data-countryCode='NP' value='977'>Nepal (+977)</option> <option data-countryCode='NL' value='31'>Netherlands (+31)</option> <option data-countryCode='NC' value='687'>New Caledonia (+687)</option> <option data-countryCode='NZ' value='64'>New Zealand (+64)</option> <option data-countryCode='NI' value='505'>Nicaragua (+505)</option> <option data-countryCode='NE' value='227'>Niger (+227)</option> <option data-countryCode='NG' value='234'>Nigeria (+234)</option> <option data-countryCode='NU' value='683'>Niue (+683)</option> <option data-countryCode='NF' value='672'>Norfolk Islands (+672) </option> <option data-countryCode='NP' value='670'>Northern Marianas (+670) </option> <option data-countryCode='NO' value='47'>Norway (+47)</option> <option data-countryCode='OM' value='968'>Oman (+968)</option> <option data-countryCode='PW' value='680'>Palau (+680)</option> <option data-countryCode='PA' value='507'>Panama (+507)</option> <option data-countryCode='PG' value='675'>Papua New Guinea (+675) </option> <option data-countryCode='PY' value='595'>Paraguay (+595)</option> <option data-countryCode='PE' value='51'>Peru (+51)</option> <option data-countryCode='PH' value='63'>Philippines (+63)</option> <option data-countryCode='PL' value='48'>Poland (+48)</option> <option data-countryCode='PT' value='351'>Portugal (+351)</option> <option data-countryCode='PR' value='1787'>Puerto Rico (+1787)</option> <option data-countryCode='QA' value='974'>Qatar (+974)</option> <option data-countryCode='RE' value='262'>Reunion (+262)</option> <option data-countryCode='RO' value='40'>Romania (+40)</option> <option data-countryCode='RU' value='7'>Russia (+7)</option> <option data-countryCode='RW' value='250'>Rwanda (+250)</option> <option data-countryCode='SM' value='378'>San Marino (+378)</option> <option data-countryCode='ST' value='239'>Sao Tome &amp; Principe (+239) </option> <option data-countryCode='SA' value='966'>Saudi Arabia (+966)</option> <option data-countryCode='SN' value='221'>Senegal (+221)</option> <option data-countryCode='CS' value='381'>Serbia (+381)</option> <option data-countryCode='SC' value='248'>Seychelles (+248)</option> <option data-countryCode='SL' value='232'>Sierra Leone (+232)</option> <option data-countryCode='SG' value='65'>Singapore (+65)</option> <option data-countryCode='SK' value='421'>Slovak Republic (+421) </option> <option data-countryCode='SI' value='386'>Slovenia (+386)</option> <option data-countryCode='SB' value='677'>Solomon Islands (+677) </option>" +
        "<option data-countryCode='SO' value='252'>Somalia (+252)</option> <option data-countryCode='ZA' value='27'>South Africa (+27)</option> <option data-countryCode='ES' value='34'>Spain (+34)</option> <option data-countryCode='LK' value='94'>Sri Lanka (+94)</option> <option data-countryCode='SH' value='290'>St. Helena (+290)</option> <option data-countryCode='KN' value='1869'>St. Kitts (+1869)</option> <option data-countryCode='SC' value='1758'>St. Lucia (+1758)</option> <option data-countryCode='SD' value='249'>Sudan (+249)</option> <option data-countryCode='SR' value='597'>Suriname (+597)</option> <option data-countryCode='SZ' value='268'>Swaziland (+268)</option> <option data-countryCode='SE' value='46'>Sweden (+46)</option> <option data-countryCode='CH' value='41'>Switzerland (+41)</option> <option data-countryCode='SI' value='963'>Syria (+963)</option> <option data-countryCode='TW' value='886'>Taiwan (+886)</option> <option data-countryCode='TJ' value='7'>Tajikstan (+7)</option> <option data-countryCode='TH' value='66'>Thailand (+66)</option> <option data-countryCode='TG' value='228'>Togo (+228)</option> <option data-countryCode='TO' value='676'>Tonga (+676)</option> <option data-countryCode='TT' value='1868'>Trinidad &amp; Tobago (+1868) </option> <option data-countryCode='TN' value='216'>Tunisia (+216)</option> <option data-countryCode='TR' value='90'>Turkey (+90)</option> <option data-countryCode='TM' value='7'>Turkmenistan (+7)</option> <option data-countryCode='TM' value='993'>Turkmenistan (+993)</option> <option data-countryCode='TC' value='1649'>Turks &amp; Caicos Islands     (+1649)</option> <option data-countryCode='TV' value='688'>Tuvalu (+688)</option> <option data-countryCode='UG' value='256'>Uganda (+256)</option> <option data-countryCode='GB' value='44'>UK (+44)</option> <option data-countryCode='UA' value='380'>Ukraine (+380)</option> <option data-countryCode='AE' value='971'>United Arab Emirates (+971) </option> <option data-countryCode='UY' value='598'>Uruguay (+598)</option> <option data-countryCode='US' value='1'>USA (+1)</option> <option data-countryCode='UZ' value='7'>Uzbekistan (+7)</option> <option data-countryCode='VU' value='678'>Vanuatu (+678)</option> <option data-countryCode='VA' value='379'>Vatican City (+379)</option> <option data-countryCode='VE' value='58'>Venezuela (+58)</option> <option data-countryCode='VN' value='84'>Vietnam (+84)</option> <option data-countryCode='VG' value='84'>Virgin Islands - British     (+1284)</option> <option data-countryCode='VI' value='84'>Virgin Islands - US (+1340) </option> <option data-countryCode='WF' value='681'>Wallis &amp; Futuna (+681) </option> <option data-countryCode='YE' value='969'>Yemen (North)(+969)</option> <option data-countryCode='YE' value='967'>Yemen (South)(+967)</option> <option data-countryCode='ZM' value='260'>Zambia (+260)</option> <option data-countryCode='ZW' value='263'>Zimbabwe (+263)</option></select><input type='text' required class='form-control width-40' id='mobileformyournumber'></div></div></div><div class='row'><div class='col-md-12'>Recipient Mobile Number</div><div class='col-md-12'><div class='input-group mb-3'>" +
        "<select class='form-control' required id='mobileformrecipientnumbercode'><option data-countryCode='DZ' value='213'>Algeria (+213)</option><option data-countryCode='AD' value='376'>Andorra (+376)</option><option data-countryCode='AO' value='244'>Angola (+244)</option><option data-countryCode='AI' value='1264'>Anguilla (+1264)</option><option data-countryCode='AG' value='1268'>Antigua &amp; Barbuda (+1268)</option><option data-countryCode='AR' value='54'>Argentina (+54)</option><option data-countryCode='AM' value='374'>Armenia (+374)</option><option data-countryCode='AW' value='297'>Aruba (+297)</option><option data-countryCode='AU' value='61'>Australia (+61)</option><option data-countryCode='AT' value='43'>Austria (+43)</option> <option data-countryCode='AZ' value='994'>Azerbaijan (+994)</option> <option data-countryCode='BS' value='1242'>Bahamas (+1242)</option> <option data-countryCode='BH' value='973'>Bahrain (+973)</option> <option data-countryCode='BD' value='880'>Bangladesh (+880)</option> <option data-countryCode='BB' value='1246'>Barbados (+1246)</option> <option data-countryCode='BY' value='375'>Belarus (+375)</option> <option data-countryCode='BE' value='32'>Belgium (+32)</option> <option data-countryCode='BZ' value='501'>Belize (+501)</option> <option data-countryCode='BJ' value='229'>Benin (+229)</option> <option data-countryCode='BM' value='1441'>Bermuda (+1441)</option> <option data-countryCode='BT' value='975'>Bhutan (+975)</option> <option data-countryCode='BO' value='591'>Bolivia (+591)</option> <option data-countryCode='BA' value='387'>Bosnia Herzegovina (+387) </option> <option data-countryCode='BW' value='267'>Botswana (+267)</option> <option data-countryCode='BR' value='55'>Brazil (+55)</option> <option data-countryCode='BN' value='673'>Brunei (+673)</option> <option data-countryCode='BG' value='359'>Bulgaria (+359)</option> <option data-countryCode='BF' value='226'>Burkina Faso (+226)</option> <option data-countryCode='BI' value='257'>Burundi (+257)</option> <option data-countryCode='KH' value='855'>Cambodia (+855)</option> <option data-countryCode='CM' value='237'>Cameroon (+237)</option> <option data-countryCode='CA' value='1'>Canada (+1)</option> <option data-countryCode='CV' value='238'>Cape Verde Islands (+238) </option> <option data-countryCode='KY' value='1345'>Cayman Islands (+1345) </option> <option data-countryCode='CF' value='236'>Central African Republic     (+236)</option> <option data-countryCode='CL' value='56'>Chile (+56)</option> <option data-countryCode='CN' value='86'>China (+86)</option> <option data-countryCode='CO' value='57'>Colombia (+57)</option> <option data-countryCode='KM' value='269'>Comoros (+269)</option> <option data-countryCode='CG' value='242'>Congo (+242)</option> <option data-countryCode='CK' value='682'>Cook Islands (+682)</option> <option data-countryCode='CR' value='506'>Costa Rica (+506)</option> <option data-countryCode='HR' value='385'>Croatia (+385)</option> <option data-countryCode='CU' value='53'>Cuba (+53)</option> <option data-countryCode='CY' value='90392'>Cyprus North (+90392) </option> <option data-countryCode='CY' value='357'>Cyprus South (+357)</option> <option data-countryCode='CZ' value='42'>Czech Republic (+42)</option> <option data-countryCode='DK' value='45'>Denmark (+45)</option> <option data-countryCode='DJ' value='253'>Djibouti (+253)</option> <option data-countryCode='DM' value='1809'>Dominica (+1809)</option> <option data-countryCode='DO' value='1809'>Dominican Republic (+1809) </option> <option data-countryCode='EC' value='593'>Ecuador (+593)</option> <option data-countryCode='EG' value='20'>Egypt (+20)</option> <option data-countryCode='SV' value='503'>El Salvador (+503)</option> <option data-countryCode='GQ' value='240'>Equatorial Guinea (+240) </option> <option data-countryCode='ER' value='291'>Eritrea (+291)</option> <option data-countryCode='EE' value='372'>Estonia (+372)</option> <option data-countryCode='ET' value='251'>Ethiopia (+251)</option> <option data-countryCode='FK' value='500'>Falkland Islands (+500) </option> <option data-countryCode='FO' value='298'>Faroe Islands (+298)</option> <option data-countryCode='FJ' value='679'>Fiji (+679)</option> <option data-countryCode='FI' value='358'>Finland (+358)</option> <option data-countryCode='FR' value='33'>France (+33)</option> <option data-countryCode='GF' value='594'>French Guiana (+594)</option> <option data-countryCode='PF' value='689'>French Polynesia (+689) </option> <option data-countryCode='GA' value='241'>Gabon (+241)</option> <option data-countryCode='GM' value='220'>Gambia (+220)</option> <option data-countryCode='GE' value='7880'>Georgia (+7880)</option> <option data-countryCode='DE' value='49'>Germany (+49)</option> <option data-countryCode='GH' value='233'>Ghana (+233)</option> <option data-countryCode='GI' value='350'>Gibraltar (+350)</option> <option data-countryCode='GR' value='30'>Greece (+30)</option> <option data-countryCode='GL' value='299'>Greenland (+299)</option> <option data-countryCode='GD' value='1473'>Grenada (+1473)</option> <option data-countryCode='GP' value='590'>Guadeloupe (+590)</option> <option data-countryCode='GU' value='671'>Guam (+671)</option> <option data-countryCode='GT' value='502'>Guatemala (+502)</option> <option data-countryCode='GN' value='224'>Guinea (+224)</option> <option data-countryCode='GW' value='245'>Guinea - Bissau (+245) </option> <option data-countryCode='GY' value='592'>Guyana (+592)</option> <option data-countryCode='HT' value='509'>Haiti (+509)</option> <option data-countryCode='HN' value='504'>Honduras (+504)</option> <option data-countryCode='HK' value='852'>Hong Kong (+852)</option>" +
        "<option data-countryCode='HU' value='36'>Hungary (+36)</option> <option data-countryCode='IS' value='354'>Iceland (+354)</option> <option data-countryCode='IN' value='91'>India (+91)</option> <option data-countryCode='ID' value='62'>Indonesia (+62)</option> <option data-countryCode='IR' value='98'>Iran (+98)</option> <option data-countryCode='IQ' value='964'>Iraq (+964)</option> <option data-countryCode='IE' value='353'>Ireland (+353)</option> <option data-countryCode='IL' value='972'>Israel (+972)</option> <option data-countryCode='IT' value='39'>Italy (+39)</option> <option data-countryCode='JM' value='1876'>Jamaica (+1876)</option> <option data-countryCode='JP' value='81'>Japan (+81)</option> <option data-countryCode='JO' value='962'>Jordan (+962)</option> <option data-countryCode='KZ' value='7'>Kazakhstan (+7)</option> <option data-countryCode='KE' value='254'>Kenya (+254)</option> <option data-countryCode='KI' value='686'>Kiribati (+686)</option> <option data-countryCode='KP' value='850'>Korea North (+850)</option> <option data-countryCode='KR' value='82'>Korea South (+82)</option> <option data-countryCode='KW' value='965'>Kuwait (+965)</option> <option data-countryCode='KG' value='996'>Kyrgyzstan (+996)</option> <option data-countryCode='LA' value='856'>Laos (+856)</option> <option data-countryCode='LV' value='371'>Latvia (+371)</option> <option data-countryCode='LB' value='961'>Lebanon (+961)</option> <option data-countryCode='LS' value='266'>Lesotho (+266)</option> <option data-countryCode='LR' value='231'>Liberia (+231)</option> <option data-countryCode='LY' value='218'>Libya (+218)</option> <option data-countryCode='LI' value='417'>Liechtenstein (+417)</option> <option data-countryCode='LT' value='370'>Lithuania (+370)</option> <option data-countryCode='LU' value='352'>Luxembourg (+352)</option> <option data-countryCode='MO' value='853'>Macao (+853)</option> <option data-countryCode='MK' value='389'>Macedonia (+389)</option> <option data-countryCode='MG' value='261'>Madagascar (+261)</option> <option data-countryCode='MW' value='265'>Malawi (+265)</option> <option data-countryCode='MY' value='60'>Malaysia (+60)</option> <option data-countryCode='MV' value='960'>Maldives (+960)</option> <option data-countryCode='ML' value='223'>Mali (+223)</option> <option data-countryCode='MT' value='356'>Malta (+356)</option> <option data-countryCode='MH' value='692'>Marshall Islands (+692) </option> <option data-countryCode='MQ' value='596'>Martinique (+596)</option> <option data-countryCode='MR' value='222'>Mauritania (+222)</option> <option data-countryCode='YT' value='269'>Mayotte (+269)</option> <option data-countryCode='MX' value='52'>Mexico (+52)</option> <option data-countryCode='FM' value='691'>Micronesia (+691)</option> <option data-countryCode='MD' value='373'>Moldova (+373)</option> <option data-countryCode='MC' value='377'>Monaco (+377)</option> <option data-countryCode='MN' value='976'>Mongolia (+976)</option> <option data-countryCode='MS' value='1664'>Montserrat (+1664)</option> <option data-countryCode='MA' value='212'>Morocco (+212)</option> <option data-countryCode='MZ' value='258'>Mozambique (+258)</option> <option data-countryCode='MN' value='95'>Myanmar (+95)</option> <option data-countryCode='NA' value='264'>Namibia (+264)</option> <option data-countryCode='NR' value='674'>Nauru (+674)</option> <option data-countryCode='NP' value='977'>Nepal (+977)</option> <option data-countryCode='NL' value='31'>Netherlands (+31)</option> <option data-countryCode='NC' value='687'>New Caledonia (+687)</option> <option data-countryCode='NZ' value='64'>New Zealand (+64)</option> <option data-countryCode='NI' value='505'>Nicaragua (+505)</option> <option data-countryCode='NE' value='227'>Niger (+227)</option> <option data-countryCode='NG' value='234'>Nigeria (+234)</option> <option data-countryCode='NU' value='683'>Niue (+683)</option> <option data-countryCode='NF' value='672'>Norfolk Islands (+672) </option> <option data-countryCode='NP' value='670'>Northern Marianas (+670) </option> <option data-countryCode='NO' value='47'>Norway (+47)</option> <option data-countryCode='OM' value='968'>Oman (+968)</option> <option data-countryCode='PW' value='680'>Palau (+680)</option> <option data-countryCode='PA' value='507'>Panama (+507)</option> <option data-countryCode='PG' value='675'>Papua New Guinea (+675) </option> <option data-countryCode='PY' value='595'>Paraguay (+595)</option> <option data-countryCode='PE' value='51'>Peru (+51)</option> <option data-countryCode='PH' value='63'>Philippines (+63)</option> <option data-countryCode='PL' value='48'>Poland (+48)</option> <option data-countryCode='PT' value='351'>Portugal (+351)</option> <option data-countryCode='PR' value='1787'>Puerto Rico (+1787)</option> <option data-countryCode='QA' value='974'>Qatar (+974)</option> <option data-countryCode='RE' value='262'>Reunion (+262)</option> <option data-countryCode='RO' value='40'>Romania (+40)</option> <option data-countryCode='RU' value='7'>Russia (+7)</option> <option data-countryCode='RW' value='250'>Rwanda (+250)</option> <option data-countryCode='SM' value='378'>San Marino (+378)</option> <option data-countryCode='ST' value='239'>Sao Tome &amp; Principe (+239) </option> <option data-countryCode='SA' value='966'>Saudi Arabia (+966)</option> <option data-countryCode='SN' value='221'>Senegal (+221)</option> <option data-countryCode='CS' value='381'>Serbia (+381)</option> <option data-countryCode='SC' value='248'>Seychelles (+248)</option> <option data-countryCode='SL' value='232'>Sierra Leone (+232)</option> <option data-countryCode='SG' value='65'>Singapore (+65)</option> <option data-countryCode='SK' value='421'>Slovak Republic (+421) </option> <option data-countryCode='SI' value='386'>Slovenia (+386)</option> <option data-countryCode='SB' value='677'>Solomon Islands (+677) </option>" +
        "<option data-countryCode='SO' value='252'>Somalia (+252)</option> <option data-countryCode='ZA' value='27'>South Africa (+27)</option> <option data-countryCode='ES' value='34'>Spain (+34)</option> <option data-countryCode='LK' value='94'>Sri Lanka (+94)</option> <option data-countryCode='SH' value='290'>St. Helena (+290)</option> <option data-countryCode='KN' value='1869'>St. Kitts (+1869)</option> <option data-countryCode='SC' value='1758'>St. Lucia (+1758)</option> <option data-countryCode='SD' value='249'>Sudan (+249)</option> <option data-countryCode='SR' value='597'>Suriname (+597)</option> <option data-countryCode='SZ' value='268'>Swaziland (+268)</option> <option data-countryCode='SE' value='46'>Sweden (+46)</option> <option data-countryCode='CH' value='41'>Switzerland (+41)</option> <option data-countryCode='SI' value='963'>Syria (+963)</option> <option data-countryCode='TW' value='886'>Taiwan (+886)</option> <option data-countryCode='TJ' value='7'>Tajikstan (+7)</option> <option data-countryCode='TH' value='66'>Thailand (+66)</option> <option data-countryCode='TG' value='228'>Togo (+228)</option> <option data-countryCode='TO' value='676'>Tonga (+676)</option> <option data-countryCode='TT' value='1868'>Trinidad &amp; Tobago (+1868) </option> <option data-countryCode='TN' value='216'>Tunisia (+216)</option> <option data-countryCode='TR' value='90'>Turkey (+90)</option> <option data-countryCode='TM' value='7'>Turkmenistan (+7)</option> <option data-countryCode='TM' value='993'>Turkmenistan (+993)</option> <option data-countryCode='TC' value='1649'>Turks &amp; Caicos Islands     (+1649)</option> <option data-countryCode='TV' value='688'>Tuvalu (+688)</option> <option data-countryCode='UG' value='256'>Uganda (+256)</option> <option data-countryCode='GB' value='44'>UK (+44)</option> <option data-countryCode='UA' value='380'>Ukraine (+380)</option> <option data-countryCode='AE' value='971'>United Arab Emirates (+971) </option> <option data-countryCode='UY' value='598'>Uruguay (+598)</option> <option data-countryCode='US' value='1'>USA (+1)</option> <option data-countryCode='UZ' value='7'>Uzbekistan (+7)</option> <option data-countryCode='VU' value='678'>Vanuatu (+678)</option> <option data-countryCode='VA' value='379'>Vatican City (+379)</option> <option data-countryCode='VE' value='58'>Venezuela (+58)</option> <option data-countryCode='VN' value='84'>Vietnam (+84)</option> <option data-countryCode='VG' value='84'>Virgin Islands - British     (+1284)</option> <option data-countryCode='VI' value='84'>Virgin Islands - US (+1340) </option> <option data-countryCode='WF' value='681'>Wallis &amp; Futuna (+681) </option> <option data-countryCode='YE' value='969'>Yemen (North)(+969)</option> <option data-countryCode='YE' value='967'>Yemen (South)(+967)</option> <option data-countryCode='ZM' value='260'>Zambia (+260)</option> <option data-countryCode='ZW' value='263'>Zimbabwe (+263)</option></select>" +
        "<input type='text' required class='form-control width-40' id='mobileformrecipientnumber'></div></div></div><div class='row'><div class='col-md-12'>Transaction Type</div><div class='col-md-12'><select class='form-control' required id='mobileformtxntype'><option value='Ecobank Transactions'>Ecobank Transactions</option><option value='Masterpass/Mvisa'>Masterpass/Mvisa</option><option value='op Ups & Bill Payments'>Top Ups & Bill Payments</option><option value='Other Bank Transactions'>Other Bank Transactions</option><option value='Inter Affiliate Transfer'>Inter Affiliate Transfer</option></select></div></div>" +
        "<div class='row'><div class='col-md-12'>Transaction Date</div><div class='col-md-12'><input type='date' required id='mobilemoneydate' class='form-control'></div></div>" +
        "<div class='row'><div class='col-md-12'>Amount</div><div class='col-md-12'><div class='input-group mb-3'><select class='form-control' required id='mobileformcurrency'><option value='USD'>United States Dollars</option><option value='EUR'>Euro</option><option value='GBP'>United Kingdom Pounds</option><option value='NGN'>Nigerian Naira</option><option value='DZD'>Algeria Dinars</option><option value='ARP'>Argentina Pesos</option><option value='AUD'>Australia Dollars</option><option value='ATS'>Austria Schillings</option><option value='BSD'>Bahamas Dollars</option><option value='BBD'>Barbados Dollars</option><option value='BEF'>Belgium Francs</option><option value='BMD'>Bermuda Dollars</option><option value='BRR'>Brazil Real</option><option value='BGL'>Bulgaria Lev</option><option value='CAD'>Canada Dollars</option><option value='CLP'>Chile Pesos</option><option value='CNY'>China Yuan Renmimbi</option><option value='CYP'>Cyprus Pounds</option><option value='CSK'>Czech Republic Koruna</option><option value='DKK'>Denmark Kroner</option><option value='NLG'>Dutch Guilders</option><option value='XCD'>Eastern Caribbean Dollars</option><option value='EGP'>Egypt Pounds</option><option value='FJD'>Fiji Dollars</option><option value='FIM'>Finland Markka</option><option value='FRF'>France Francs</option><option value='DEM'>Germany Deutsche Marks</option><option value='XAU'>Gold Ounces</option><option value='GRD'>Greece Drachmas</option><option value='HKD'>Hong Kong Dollars</option><option value='HUF'>Hungary Forint</option><option value='ISK'>Iceland Krona</option><option value='INR'>India Rupees</option><option value='IDR'>Indonesia Rupiah</option><option value='IEP'>Ireland Punt</option><option value='ILS'>Israel New Shekels</option><option value='ITL'>Italy Lira</option><option value='JMD'>Jamaica Dollars</option><option value='JPY'>Japan Yen</option><option value='JOD'>Jordan Dinar</option><option value='KRW'>Korea (South) Won</option><option value='LBP'>Lebanon Pounds</option><option value='LUF'>Luxembourg Francs</option><option value='MYR'>Malaysia Ringgit</option><option value='MXP'>Mexico Pesos</option><option value='NLG'>Netherlands Guilders</option><option value='NZD'>New Zealand Dollars</option><option value='NOK'>Norway Kroner</option><option value='PKR'>Pakistan Rupees</option><option value='XPD'>Palladium Ounces</option><option value='PHP'>Philippines Pesos</option><option value='XPT'>Platinum Ounces</option><option value='PLZ'>Poland Zloty</option><option value='PTE'>Portugal Escudo</option><option value='ROL'>Romania Leu</option><option value='RUR'>Russia Rubles</option><option value='SAR'>Saudi Arabia Riyal</option><option value='XAG'>Silver Ounces</option><option value='SGD'>Singapore Dollars</option><option value='SKK'>Slovakia Koruna</option><option value='ZAR'>South Africa Rand</option><option value='KRW'>South Korea Won</option><option value='ESP'>Spain Pesetas</option><option value='XDR'>Special Drawing Right (IMF)</option><option value='SDD'>Sudan Dinar</option><option value='SEK'>Sweden Krona</option><option value='CHF'>Switzerland Francs</option><option value='TWD'>Taiwan Dollars</option><option value='THB'>Thailand Baht</option><option value='TTD'>Trinidad and Tobago Dollars</option><option value='TRL'>Turkey Lira</option><option value='VEB'>Venezuela Bolivar</option><option value='ZMK'>Zambia Kwacha</option><option value='EUR'>Euro</option><option value='XCD'>Eastern Caribbean Dollars</option><option value='XDR'>Special Drawing Right (IMF)</option><option value='XAG'>Silver Ounces</option><option value='XAU'>Gold Ounces</option><option value='XPD'>Palladium Ounces</option><option value='XPT'>Platinum Ounces</option></select>" +
        "<input type='number' min='1' required class='form-control width-40' id='mobileformamount'></div></div></div><div><input type='submit' class='btn btn-primary btn-sm' value='Submit' onclick='mobilemoneyform.submitMobileForm(event)' type='submit'/></div></form></div>";

    function init() {
        var msg_container = document.getElementById("msg_container");
        var msg_body = document.getElementById("msg_card_body");
        msg_body.insertAdjacentHTML("beforeend", component);
        msg_container.scrollTop = msg_container.scrollHeight;
    }

    function submitMobileForm(event) {
        event.preventDefault();
        var message = {
            type: "MOBILE_MONEY_FORM",
            data : {
                regPhone : document.getElementById("mobileformyournumbercode").value+document.getElementById("mobileformyournumber").value,
                recipient_phone : document.getElementById("mobileformrecipientnumbercode").value+document.getElementById("mobileformrecipientnumber").value,
                transType : document.getElementById("mobileformtxntype").value,
                date : document.getElementById("mobilemoneydate").value,
                amount : document.getElementById("mobileformcurrency").value+document.getElementById("mobileformamount").value
            }
        }

        parent.onSend(message);
        var fo = document.getElementById("mmform");
        fo.remove();

    }
        mobileMoneyForm.init = init;
        mobileMoneyForm.submitMobileForm = submitMobileForm;

    }


    function handleSessionLoaded(data){
        for(var d = 0;d<data.length;d++){
            var user = data[d].sender == 'User'?true:false;
            if(user){
                if(data[d].message.length>0){
                   for(var s=0; s<data[d].message.length; s++){
                        if(data[d].message[s] != ""){
                            parent.bind_user_msgDT(data[d].message[s],data[d].transaction_date);
                        }
                   }
                    
                }
            }
            else {
                if(!data[d].has_form && ! data[d].has_suggestions){
                    parent.renderMessage(data[d]);
                } else {
                    parent.renderTextData(data[d]);
                }
                
            }
        }
    }   


    function submitOtpForm() {
        var component = "<div class='form-div' id='otpform'><div>OTP</div><div><input type='text' placeholder='OTP' required maxlength='16' name='acctNum' class='form-control' id='otp'/></div><div><input type='button' class='btn btn-primary btn-sm' value='Submit' onclick='submitotpform.submit(event)' /></div></div>";
    
        function init() {
            var msg_container = document.getElementById("msg_container");
            var msg_body = document.getElementById("msg_card_body");
            msg_body.insertAdjacentHTML("beforeend", component);
            console.log(component);
            msg_container.scrollTop = msg_container.scrollHeight;
        }
    
        function submit(event) {
            event.preventDefault();
            var OTP = document.getElementById('otp').value;
            // {"type":"VALIDATION_FORM","data":{"AFFILIATE_CODE":"ENG",
            // "ACCOUNT_NAME":null,"ACCOUNT_NUMBER":null,"PHONE":"+23407062725721","SUMMARY":null,"NAME":"Gaffar Kasumu","IS_CUSTOMER":false}}"
            const data = {
                type: "OTP_FORM",
                data: {
                    OTP:OTP
                }
            }
            parent.onSend(data);
            var fo = document.getElementById("otpform");
            fo.remove();
        }
    
        submitOtpForm.init = init;
        submitOtpForm.submit = submit;
    }

    function checkuserActivity(event){
        console.log(event);
        setTimeout(function () {
            parent.onSend({event: 'ping', data: {}});
        }, 3000);
    }

    function startKeepAlive(){
    setInterval(function(){
        console.log("Keeping Alive");
        parent.onSend({event: 'ping', data: {}});
    }, 30000)
}
startKeepAlive();
    // INITIALIZATION //

    // initSocketCon();

    // INIT END //

    // SEND_TOKEN    const x_token = req.headers['x-rafiki-auth']; { type: runtime.SEND_TOKEN, payload: x_token }